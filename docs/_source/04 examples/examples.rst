Examples
==========

Tutorials
----------

This section contains a number of exemplary simulation scripts to illustrate the use of Smuthi.
Each tutorial is supposed to illustrate a certain aspect of the software.
Click on the respective tutorial names to view a brief discussion.

=== ==================================  ============== ============
No. Tutorial                             level           script    
=== ==================================  ============== ============
1   |ex1link|                             |introd|      |ex1script|
2   |ex2link|                             |introd|      |ex2script|
3   |ex3link|                             |introd|      |ex3script| 
4   |ex4link|                             |intermed|    |ex4script|  
5   |ex5link|                             |intermed|      TBD       
6   |ex6link|                             |intermed|      TBD       
7   |ex7link|                             |advanced|    |ex7script| 
8   |ex8link|                             |advanced|    |ex8script| 
=== ==================================  ============== ============ 

.. |ex1script| replace:: :download:`download <../../../examples/tutorials/01_setting_up_a_simulation/dielectric_sphere_on_substrate.py>`

.. |ex1link| replace:: :doc:`Setting up a simulation <01_setting_up_a_simulation/discussion>`


.. |ex2script| replace:: :download:`download <../../../examples/tutorials/02_plotting_the_near_field/three_spheres_in_waveguide.py>`

.. |ex2link| replace:: :doc:`Plotting the near field <02_plotting_the_near_field/discussion>`


.. |ex3script| replace:: :download:`download <../../../examples/tutorials/03_plotting_the_far_field/fifteen_spheres_on_substrate.py>`

.. |ex3link| replace:: :doc:`Plotting the far field <03_plotting_the_far_field/discussion>`


.. |ex4script| replace:: :download:`download <../../../examples/tutorials/04_non_spherical_particles/non_spherical_particles.zip>`

.. |ex4link| replace:: :doc:`Non-spherical particles <04_non_spherical_particles/discussion>`


.. |ex5link| replace:: Dipole sources


.. |ex6link| replace:: Gaussian beams


.. |ex7link| replace:: :doc:`Automatic parameter selection <07_automatic_parameter_selection/discussion>`

.. |ex7script| replace:: :download:`download <../../../examples/tutorials/07_automatic_parameter_selection/nine_disks_on_a_thin_film_system.py>`


.. |ex8script| replace:: :download:`download <../../../examples/tutorials/08_many_particle_simulations/many_dielectric_spheres_on_substrate.py>`

.. |ex8link| replace:: :doc:`Many particle simulations <08_many_particles/discussion>`



.. |introd| raw:: html

    <font color="green">introductory</font>

.. |intermed| raw:: html

    <font color="orange">intermediate</font>

.. |advanced| raw:: html

    <font color="red">advanced</font>


Benchmarks
-----------

This section contains a number of benchmarks between Smuthi and other codes 
with regard to accuracy and/or runtime.
Click on the respective benchmark names to view a brief discussion.

=== ================================ ============== ===================
No. Benchmark                         other method   script and data    
=== ================================ ============== ===================
1   |bench1link|                      FEM            |bench1data|                
=== ================================ ============== =================== 

.. |bench1link| replace:: :doc:`Four particles in slab waveguide <four_particles_in_slab/discussion>`

.. |bench1data| replace:: :download:`download <../../../examples/benchmarks/four_particles_in_slab/four_particles_in_slab.zip>`
